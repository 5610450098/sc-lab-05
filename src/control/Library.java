package control;

import java.util.ArrayList;

import model.Book;
import model.People;
import model.Personnel;
import model.Student;
import model.Teacher;

public class Library {
	private ArrayList<Book> bookList = new ArrayList<Book>();
	private ArrayList<People> peopleList = new ArrayList<People>();
	
	public String showDetailBooks(){
		String str = "";
		for(Book books : bookList){
			if(books.checkStatusBook()){
				str = str+books.getIdBook()+"\t"+books.getNameBook()+"\t"+books.nameUser()+"\n";
			}
			else{
				str = str+books.getIdBook()+"\t"+books.getNameBook()+"\t\n";
			}
		}
		return str ;
	}
	
	public void addBook(String name, String id){
		bookList.add(new Book(name, id));
		
	}
	public void addStudent(String name, String id){
		peopleList.add(new Student(name, id));
		
	}
	public void addTeacher(String name, String id){
		peopleList.add(new Teacher(name, id));
		
	}
	public void addPersonnel(String name,String id){
		peopleList.add(new Personnel(name, id));
		
	}
	public void borrowBook(String idPeople,String idBook){
		for(People people : peopleList){
			if(people.getIdPeople().equals(idPeople)){
				for(Book book : bookList){
					if(book.getIdBook().equals(idBook)){
						book.changeStatusBook();
						book.userBoorow(people.getNamePeople());
						people.peopleBorrowBook(idBook);
					}
				}
				
			}
			
		}
		
	}
	public void returnBook(String idPeople,String idBook){
		for(People people : peopleList){
			if(people.getIdPeople().equals(idPeople)){
				for(Book book : bookList){
					if(book.getIdBook().equals(idBook)){
						book.changeStatusBook();
						book.userReturn();
						people.peopleReturnBook(idBook);
					}
				}
				
			}
		}
		
	}
	public String showNameAndIdPeople(String idPeople){
		for (People people : peopleList){
			if(people.getIdPeople().equals(idPeople)){
				return people.getIdPeople()+"  :  "+people.getNamePeople()+"\nDetail Borrow Book :\n"+people.showDetailBorrow();
			}
//			return people.getIdPeople()+"/t"+people.getNamePeople();
		}
		return "No Found";

	}
		
	
}
