package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;


import viewer.Gui;

public class LibraryC {
	private Gui frame;
	private Library library;
	

	public LibraryC() {
		// TODO Auto-generated constructor stub
		library = new Library();
		frame = new Gui();
		frame.setVisible(true);
		frame.setSize(550, 500);
		frame.setLocation(120, 80);
		frame.setListennerAddPeopleBT(new AddPeopleBTListenner());
		frame.setListennerAddBookBT(new AddBookBTListenner());
		frame.setListennerSearchIdPeople(new AddSearchIdPeopleListenner());
		frame.setListennerBoorowBook(new AddBoorowBookListenner());
		frame.setListennerReturnBook(new AddReturnBookListenner());
		frame.setListtnnerRefreshDetailBooks(new AddRefreshDetailListener());
		testCase();
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new LibraryC();

//		Calendar c = Calendar.getInstance();
//		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		String currentDate = df.format(c.getTime());
//		System.out.println("Current Date : " + currentDate);
	}
	
	public void testCase(){
		library.addStudent("wut", "5610451191");
		library.addBook("Big Java", "C217");
		library.addBook("����Ԩ��", "X999");
		library.borrowBook("5610451191", "C217");
		library.borrowBook("5610451191", "X999");
		library.addBook("Calculus", "M215");
		library.addTeacher("pond", "1234567890");
		library.borrowBook("1234567890", "X999");
	}
	
	class AddPeopleBTListenner implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(!frame.getTextIdPeople().equals("") || !frame.getTextNamePeople().equals("")){
				if(frame.selectRB() == "Personnel" && frame.getTextIdPeople() != " "){
					library.addPersonnel(frame.getTextNamePeople(), frame.getTextIdPeople());
				}
				else if(frame.selectRB() == "Student"){
					library.addStudent(frame.getTextNamePeople(), frame.getTextIdPeople());
				}
				else if(frame.selectRB() == "Teacher"){
					library.addTeacher(frame.getTextNamePeople(), frame.getTextIdPeople());
				}
				else{
					
				}


			}
			
		}
		
	}
	class AddBookBTListenner implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
//			System.out.println(library.peopleList.get(0).idPeople);
			if (!frame.getTextIdBook().equals("") || !frame.getTextNameBook().equals("")){
				library.addBook(frame.getTextNameBook(), frame.getTextIdBook());
			}
			
		}
		
	}
	class AddSearchIdPeopleListenner implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setResultDetail(library.showNameAndIdPeople(frame.getIdPeopleSearch()));
		}
		
	}
	class AddBoorowBookListenner implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(!frame.getIdPeopleSearch().equals("")){
				library.borrowBook(frame.getIdPeopleSearch(), frame.getTextIdBookBorrow());
				frame.setResultDetail(library.showNameAndIdPeople(frame.getIdPeopleSearch()));
			}
			
		}
		
	}
	class AddReturnBookListenner implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(!frame.getIdPeopleSearch().equals("")){
				library.returnBook(frame.getIdPeopleSearch(), frame.getTextIdBookReturn());
				frame.setResultDetail(library.showNameAndIdPeople(frame.getIdPeopleSearch()));
			}
		}
		
	
	}
	class AddRefreshDetailListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setTextDetailBooks(library.showDetailBooks());
		}
	}	


}
