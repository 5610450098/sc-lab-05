package model;

public class Book {
	private String idBook;
	private String nameBook;
	private boolean statusBook;
	private String user;
	
	
	public Book(String name, String id){
		nameBook = name;
		idBook = id;
		statusBook = false;
		user = "";
	}
	
	public String getNameBook(){
		return nameBook;
		
	}
	
	public String getIdBook(){
		return idBook;
	}
	
	public boolean checkStatusBook(){
		return statusBook;
	}
	public void changeStatusBook(){
		if(statusBook == false){
			statusBook = true;
		}
		else {
			statusBook = false;
		}
		
	}
	public void userBoorow(String nameUser){
		user = nameUser;
	}
	public void userReturn(){
		user = "";
	}
	public String nameUser(){
		return user;
	}
	
	
	

}
