package model;

import java.util.ArrayList;

public abstract class People {
	protected String namePeople;
	protected String idPeople;
	protected ArrayList<String> bookBorrowList ;
	
	
	public People(String name, String id){
		namePeople = name;
		idPeople = id;
		bookBorrowList = new ArrayList<String>();
		
	}
	public String getNamePeople(){
		return namePeople;
	}
	public String getIdPeople(){
		return idPeople;
	}
	public abstract void peopleBorrowBook(String name);
	public void peopleReturnBook(String idBook){
		for(int i=0;i<bookBorrowList.size();i++){
			if(idBook.equals(bookBorrowList.get(i))){
				bookBorrowList.remove(i);
			}
		}
	}
	public String showDetailBorrow(){
		String str = "";
		for (String detailBorrow : bookBorrowList){
			str = str+detailBorrow+"\n";
		}
		return str;
	}
	

}
