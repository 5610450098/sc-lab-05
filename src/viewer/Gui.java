package viewer;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class Gui extends JFrame {
	private JButton addBookBT;
	private JButton addPeopleBT;
	private JTextField nameBookField;
	private JTextField idBookField;
	private JTextField namePeopleField;
	private JTextField idPeopleField;
	private JRadioButton personnelRB;
	private JRadioButton teacherRB;
	private JRadioButton studentRB;
	private JTextArea resultDetail;
	private JButton searchIdpeopleBT;
	private JTextField idPeopleBorrowF;
	private JTextField idBookBorrowF;
	private JButton borrowBookBT;
	private JTextField idBookReturnF;
	private JButton returnBookBT;
	private JTextArea detailBooksA;
	private JButton refreshDetailBT;
	
	public Gui(){
		createFrame();
	}
	
	public void createFrame(){
		JLabel nameLibraryLB = new JLabel("Library");
		addPeopleBT = new JButton("Add");
		addBookBT = new JButton("Add");
		nameBookField = new JTextField(10);
		idBookField = new JTextField(10);
		namePeopleField = new JTextField(10);
		idPeopleField = new JTextField(10);
		resultDetail = new JTextArea();
		JScrollPane scrollDetail = new JScrollPane(resultDetail);
		JLabel idPeopleBorrowL = new JLabel("ID People");
		idPeopleBorrowF = new JTextField(10);
		searchIdpeopleBT = new JButton("Search");
		JLabel idBookBorrowL = new JLabel("ID Book");
		idBookBorrowF = new JTextField(10);
		borrowBookBT = new JButton("Borrow");
		JLabel idBookReturnL = new JLabel("ID Book");
		idBookReturnF = new JTextField(10);
		returnBookBT = new JButton("Return");
		detailBooksA = new JTextArea("Detail Books :\nID Book\tName Book\tUser\n",24,40);
		JScrollPane scrollDetailBooks = new JScrollPane(detailBooksA);
		refreshDetailBT = new JButton("refresh");
		
		JTabbedPane tabPane = new JTabbedPane();
		JPanel addPanel = new JPanel();
		JPanel bookBRP = new JPanel();
		JPanel detailBooks = new JPanel();
		tabPane.addTab("Borrow & Return Books", null, bookBRP, null);
		tabPane.addTab("Add Book & Add People", null, addPanel, null);
		tabPane.addTab("Detail Books", null, detailBooks, null);
		
		bookBRP.setLayout(new GridLayout(1,2));
		addPanel.setLayout(new GridLayout(1,2));
//		detailBooks.setLayout(new GridLayout(1,1));
		
		JPanel addBookP = new JPanel();
//		addBookP.setLayout(new GridLayout(3,1));
		JPanel addPeopleP = new JPanel();
//		addPeopleP.setLayout(new GridLayout(4,1));
		JLabel nameBookL = new JLabel("Name Book");
		JLabel idBookL = new JLabel("ID Book");
		JLabel namePeopleL = new JLabel("Name People"); 
		JLabel idPeopleL = new JLabel("ID People");
		
		personnelRB = new JRadioButton("Personnel");
		teacherRB = new JRadioButton("Teacher");
		studentRB = new JRadioButton("Student");
		ButtonGroup groupRB = new ButtonGroup();
		groupRB.add(studentRB);
		groupRB.add(teacherRB);
		groupRB.add(personnelRB);
		studentRB.setSelected(true);
		
		JPanel RBPanel = new JPanel();
		RBPanel.add(studentRB);
		RBPanel.add(teacherRB);
		RBPanel.add(personnelRB);
		RBPanel.setBorder(new TitledBorder(new EtchedBorder(), "User"));
		
		JPanel bookNameP = new JPanel();
		JPanel bookIdP = new JPanel();
		JPanel bookBTP = new JPanel();
		
		bookNameP.add(nameBookL);
		bookNameP.add(nameBookField);
		bookIdP.add(idBookL);
		bookIdP.add(idBookField);
		bookBTP.add(addBookBT);
		
		JPanel peopleNameP = new JPanel();
		JPanel peopleIdP = new JPanel();
		JPanel peopleBTP = new JPanel();
		
		peopleNameP.add(namePeopleL);
		peopleNameP.add(namePeopleField);
		peopleIdP.add(idPeopleL);
		peopleIdP.add(idPeopleField);
		peopleBTP.add(addPeopleBT);
		
		addPeopleP.add(peopleNameP);
		addPeopleP.add(peopleIdP);
		addPeopleP.add(RBPanel);
		addPeopleP.add(peopleBTP);
		
		addBookP.add(bookNameP);
		addBookP.add(bookIdP);
		addBookP.add(bookBTP);
		
		addPanel.add(addBookP);
		addPanel.add(addPeopleP);
		
		JPanel detailBorrowP = new JPanel(); 
		JPanel idPeopleDetailP = new JPanel();
		idPeopleDetailP.add(idPeopleBorrowL);
		idPeopleDetailP.add(idPeopleBorrowF);
		idPeopleDetailP.add(searchIdpeopleBT);
		JPanel borrowP = new JPanel();
		borrowP.setBorder(new TitledBorder(new EtchedBorder(), "Borrow Books"));
		borrowP.add(idBookBorrowL);
		borrowP.add(idBookBorrowF);
		borrowP.add(borrowBookBT);
		JPanel returnP = new JPanel();
		returnP.setBorder(new TitledBorder(new EtchedBorder(), "Return Books"));
		returnP.add(idBookReturnL);
		returnP.add(idBookReturnF);
		returnP.add(returnBookBT);
		
		detailBorrowP.add(idPeopleDetailP);
		JPanel detailB_RP = new JPanel();
		detailB_RP.setLayout(new GridLayout(3,1));
		detailB_RP.add(detailBorrowP);
		detailB_RP.add(borrowP);
		detailB_RP.add(returnP);
		bookBRP.add(scrollDetail);
		bookBRP.add(detailB_RP);
		JPanel detailP = new JPanel();
		detailP.add(scrollDetailBooks, BorderLayout.CENTER);
		detailP.add(refreshDetailBT,BorderLayout.SOUTH);
//		detailBooks.add(scrollDetailBooks,BorderLayout.CENTER);
//		detailBooks.add(refreshDetailBT,BorderLayout.SOUTH);
		detailBooks.add(detailP);
		
		JPanel northPanel = new JPanel();
		northPanel.add(nameLibraryLB);
		add(northPanel,BorderLayout.NORTH);
		add(tabPane,BorderLayout.CENTER);
	}

	public void setListennerAddPeopleBT(ActionListener list){
		addPeopleBT.addActionListener(list);
		
	}
	public String getTextNameBook(){
		return nameBookField.getText();
		
	}
	public String getTextIdBook(){
		return idBookField.getText();
		
	}
	public String getTextNamePeople(){
		return namePeopleField.getText();
		
	}
	public String getTextIdPeople(){
		return idPeopleField.getText();
		
	}
	public void setListennerAddBookBT(ActionListener list){
		addBookBT.addActionListener(list);
	}
	public String selectRB(){
		if(personnelRB.isSelected()){
			return "Personnel";
			
		}
		else if(studentRB.isSelected()){
			return "Student";
			
		}
		else{
			return "Teacher";
			
		}
	
	}
	public void setResultDetail(String str){
		resultDetail.setText(str);
	}
	public String getIdPeopleSearch(){
		return idPeopleBorrowF.getText();
	}
	public void setListennerSearchIdPeople(ActionListener list){
		searchIdpeopleBT.addActionListener(list);
	}
	public String getTextIdBookBorrow(){
		return idBookBorrowF.getText();
	}
	public String getTextIdBookReturn(){
		return idBookReturnF.getText();
	}
	public void setListennerBoorowBook(ActionListener list){
		borrowBookBT.addActionListener(list);
	}
	public void setListennerReturnBook(ActionListener list){
		returnBookBT.addActionListener(list);
	}
	public void setTextDetailBooks(String str){
		detailBooksA.setText("Detail Books :\nID Book\tName Book\tUser\n"+str);
	}
	public void setListtnnerRefreshDetailBooks(ActionListener list){
		refreshDetailBT.addActionListener(list);
	}
	
}
